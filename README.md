# Como rodar o projeto

1. Clone o repositório com o comando:

```
git clone git@gitlab.com:Isacre/digital-republic-test.git
```

2. Instale as dependencias do projeto com um o comando `npm install`

3. Assim que houver finalizado a instalação, digite:

```
cd-digital-republic-test
```

e

```
npm start

```

## Pronto!
