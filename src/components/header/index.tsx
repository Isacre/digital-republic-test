import React from "react";
import "./styles.scss";
import Logo from "../../assets/logo.jpg";

export default function Header() {
  return (
    <div className="header-container">
      <img src={Logo} alt="quantas latas logo" />
    </div>
  );
}
