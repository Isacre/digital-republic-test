import React from "react";
import "./styles.scss";

export default function SquareGraph(props: any) {
  return (
    <div className="calc-area">
      <div className="outer-square">
        <div className="inner-square">
          <div>
            <div className="leftwall">{props.LeftWall.squareArea.toFixed(2)}M²</div>
            <div className="rightwall">{props.RightWall.squareArea.toFixed(2)}M²</div>
            <div className="bottomwall">{props.BackWall.squareArea.toFixed(2)}M²</div>
            <div className="topwall">{props.FrontWall.squareArea.toFixed(2)}M²</div>
          </div>
          {props.SquareArea !== 0 && <h1>{`${props.SquareArea.toFixed(2)} M²`}</h1>}
        </div>
      </div>
    </div>
  );
}
