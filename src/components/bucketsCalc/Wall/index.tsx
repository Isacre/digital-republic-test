import React from "react";
interface WallFunctionTypes {
  name: string;
  setValue: Function;
  value: any;
}
export default function Wall(props: WallFunctionTypes) {
  const { name, setValue, value } = props;
  return (
    <div className="wall-input-container">
      <h3>{name}</h3>
      <input
        className="wall-input"
        placeholder="Altura"
        value={value.height}
        onChange={(e: any) => {
          setValue({
            ...value,
            height: e.target.value,
          });
        }}
      />
      <input
        className="wall-input"
        placeholder="Largura"
        value={value.width}
        onChange={(e) =>
          setValue({
            ...value,
            width: e.target.value,
          })
        }
      />
      <input
        className="wall-input"
        placeholder="Numero de portas"
        value={value.doors}
        onChange={(e) =>
          setValue({
            ...value,
            doors: e.target.value,
          })
        }
      />
      <input
        className="wall-input"
        placeholder="Numero de janelas"
        value={value.windows}
        onChange={(e) =>
          setValue({
            ...value,
            windows: e.target.value,
          })
        }
      />
    </div>
  );
}
