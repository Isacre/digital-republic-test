import React, { useState } from "react";
import Wall from "./Wall";
import "./styles.scss";
import SquareGraph from "./SquareGraph";

interface WallType {
  height: number;
  width: number;
  windows: number;
  doors: number;
  squareArea: number;
}
export default function BucketsCalc() {
  const [LeftWall, setLeftWall] = useState<WallType>({
    height: 0,
    width: 0,
    windows: 0,
    doors: 0,
    squareArea: 0,
  });
  const [RightWall, setRightWall] = useState<WallType>({
    height: 0,
    width: 0,
    windows: 0,
    doors: 0,
    squareArea: 0,
  });
  const [BackWall, setBackWall] = useState<WallType>({
    height: 0,
    width: 0,
    windows: 0,
    doors: 0,
    squareArea: 0,
  });
  const [FrontWall, setFrontWall] = useState<WallType>({
    height: 0,
    width: 0,
    windows: 0,
    doors: 0,
    squareArea: 0,
  });
  const [SquareArea, setSquareArea] = useState<number>(0);
  const [Buckets, setBuckets] = useState<string>("Calcule quantas latas de tinta irá precisar para pintar sua casa");

  function calculatePaintBucketsNeeded(SquareAreaValue: number) {
    setBuckets("Estamos calculando quantas latas de tinta irá precisar para pintar sua casa, por favor, aguarde...");
    const paintBucketSizes = [18, 3.6, 2.5, 0.5];
    let bucketsNeeded = Array(4).fill(0);
    let value = 0;
    for (let i = 0; value < SquareAreaValue || i < paintBucketSizes.length; i++) {
      const areaPaintedByOneLitter = 5;
      const areaPaintedByOneBucket = paintBucketSizes[i] * areaPaintedByOneLitter;

      if (value + areaPaintedByOneBucket <= SquareAreaValue) {
        value = value + areaPaintedByOneBucket;
        bucketsNeeded[i]++;
        i = 0;
      }

      if (value + areaPaintedByOneBucket > SquareAreaValue) {
        if (value + areaPaintedByOneBucket <= SquareAreaValue) {
          value = value + areaPaintedByOneBucket;
          bucketsNeeded[i]++;
        }

        if (i === 3 && value < SquareAreaValue) {
          bucketsNeeded[i]++;
          value = value + areaPaintedByOneBucket;
        }
      }
    }

    const BigBucketsNeeded = bucketsNeeded[0] > 0 ? `${bucketsNeeded[0]} latas de 18L ` : "";
    const MediumBucketsNeeded = bucketsNeeded[1] > 0 ? ` ${bucketsNeeded[1]} lata(s) de 3,6L ` : "";
    const SmallBucketsNeeded = bucketsNeeded[2] > 0 ? `${bucketsNeeded[2]} lata(s) de 2,5L ` : "";
    const XSBucketsNeeded = bucketsNeeded[3] > 0 ? `e  ${bucketsNeeded[3]} lata(s) de 0.5L` : "";
    setBuckets(
      `Para pintar ${SquareAreaValue.toFixed(2)}M² será necessario comprar  ${BigBucketsNeeded}
      ${MediumBucketsNeeded}${SmallBucketsNeeded} ${XSBucketsNeeded}`
    );
  }

  function squareAreaHandler(
    frontWall: WallType,
    backWall: WallType,
    leftWall: WallType,
    rightWall: WallType,
    stateFunction: Function,
    frontWallFunction: Function,
    backWallFunction: Function,
    leftWallFunction: Function,
    rightWallFunction: Function
  ) {
    const window = {
      width: 2,
      height: 1.2,
    };
    const door = {
      width: 0.8,
      height: 1.9,
    };
    const doorArea = door.height * door.width;
    const windowArea = window.height * window.width;
    const leftWallArea = LeftWall.height * LeftWall.width - windowArea * LeftWall.windows - doorArea * LeftWall.doors;
    const rightWallArea =
      RightWall.height * RightWall.width - windowArea * RightWall.windows - doorArea * RightWall.doors;
    const backWallArea = backWall.height * backWall.width - windowArea * backWall.windows - doorArea * backWall.doors;
    const frontWallArea =
      frontWall.height * frontWall.width - windowArea * frontWall.windows - doorArea * frontWall.doors;
    frontWallFunction({ ...frontWall, squareArea: frontWallArea });
    backWallFunction({ ...backWall, squareArea: backWallArea });
    leftWallFunction({ ...leftWall, squareArea: leftWallArea });
    rightWallFunction({ ...rightWall, squareArea: rightWallArea });
    //Handles verifying if the wall is less than 1m² or more than 15m²
    if (leftWallArea < 1 || leftWallArea > 15) {
      alert(`A parede esquerda(${leftWallArea.toFixed(2)}M) deve ter entre 1 e 15 metros quadrados`);
      return;
    }
    if (rightWallArea < 1 || rightWallArea > 15) {
      alert(`A parede direita (${rightWallArea.toFixed(2)}M) deve ter entre 1 e 15 metros quadrados`);
      return;
    }

    if (backWallArea < 1 || backWallArea > 15) {
      alert(`A parede traseira(${backWallArea.toFixed(2)}M) deve ter entre 1 e 15 metros quadrados`);
      return;
    }
    if (frontWallArea < 1 || frontWallArea > 15) {
      alert(`A parede frontal(${frontWallArea.toFixed(2)}M) deve ter entre 1 e 15 metros quadrados`);
      return;
    }
    //Handles verifying if doors fit the bussiness rules

    if (frontWall.height < door.height + 0.3) {
      alert(
        `A altura da parede frontal(${frontWall.height}M) deve ser ao menos 30 centimetros maior que a altura da porta`
      );
      return;
    }
    if (leftWall.height < door.height + 0.3) {
      alert(
        `A altura da parede esquerda(${leftWall.height}M) deve ser ao menos 30 centimetros maior que a altura da porta`
      );
      return;
    }
    if (rightWall.height < door.height + 0.3) {
      alert(
        `A altura da parede direita(${rightWall.height}M) deve ser ao menos 30 centimetros maior que a altura da porta`
      );
      return;
    }
    if (backWall.height < door.height + 0.3) {
      alert(
        `A altura da parede traseira(${backWall.height}M) deve ser ao menos 30 centimetros maior que a altura da porta`
      );
      return;
    }
    if (doorArea * frontWall.doors > frontWallArea * 0.5) {
      alert(
        `A área total das portas e janelas(${
          doorArea * frontWall.doors + windowArea * frontWall.windows
        }M) deve ser menor que 50% da área da parede frontal`
      );
      return;
    }
    if (doorArea * leftWall.doors > leftWallArea * 0.5) {
      alert(
        `A área total das portas e janelas(${
          doorArea * leftWall.doors + windowArea * leftWall.windows
        }M) deve ser menor que 50% da área da parede esquerda`
      );
      return;
    }
    if (doorArea * rightWall.doors > rightWallArea * 0.5) {
      alert(
        `A área total das portas e janelas(${
          doorArea * rightWall.doors + windowArea * rightWall.windows
        }M) deve ser menor que 50% da área da parede direita`
      );
      return;
    }
    if (doorArea * backWall.doors > backWallArea * 0.5) {
      alert(
        `A área total das portas e janelas(${
          doorArea * backWall.doors + windowArea * backWall.windows
        }M) deve ser menor que 50% da área da parede traseira`
      );
      return;
    }

    stateFunction(frontWallArea + leftWallArea + rightWallArea + backWallArea);
    calculatePaintBucketsNeeded(frontWallArea + leftWallArea + rightWallArea + backWallArea);
  }

  return (
    <div className="calc-container">
      <h2 className="calc-component-header">{Buckets}</h2>
      <div className="inputs-area">
        <h3>Por favor, informe as medidas de cada parede </h3>
        <div className="inputs-container">
          <Wall name="Parede da esquerda" setValue={setLeftWall} value={LeftWall} />
          <Wall name="Parede da direita" setValue={setRightWall} value={RightWall} />
          <Wall name="Parede da frente" setValue={setFrontWall} value={FrontWall} />
          <Wall name="Parede do fundo" setValue={setBackWall} value={BackWall} />
        </div>
      </div>
      <div
        style={{ display: "flex", flexDirection: "column", gap: 10, alignItems: "center", justifyContent: "center" }}
      >
        <SquareGraph
          LeftWall={LeftWall}
          FrontWall={FrontWall}
          RightWall={RightWall}
          BackWall={BackWall}
          SquareArea={SquareArea}
        />
        <button
          className="calc-button"
          onClick={() => {
            squareAreaHandler(
              FrontWall,
              BackWall,
              LeftWall,
              RightWall,
              setSquareArea,
              setFrontWall,
              setBackWall,
              setLeftWall,
              setRightWall
            );
          }}
        >
          Calcular
        </button>
      </div>
    </div>
  );
}
