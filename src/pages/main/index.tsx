import BucketsCalc from "../../components/bucketsCalc";
import Header from "../../components/header";
import "react-toastify/dist/ReactToastify.css";
import "./styles.scss";

export default function MainPage() {
  return (
    <div className="mainpage">
      <Header />
      <div className="non-header-elements">
        <BucketsCalc />
      </div>
    </div>
  );
}
