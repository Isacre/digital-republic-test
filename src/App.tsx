import React from "react";
import MainPage from "./pages/main";
import "./styles/global.scss";

function App() {
  return (
    <div className="App">
      <MainPage />
    </div>
  );
}

export default App;
